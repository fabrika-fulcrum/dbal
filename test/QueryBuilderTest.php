<?php

use Fulcrum\Dbal\DB;
use Fulcrum\Dbal\MockConnection;
use Fulcrum\Dbal\OperationType;
use PHPUnit\Framework\TestCase;

class QueryBuilderTest extends TestCase
{

    public function setUp(): void
    {
        DB::setConnection(new MockConnection('', '', '', ''));
    }

    public function testSimpleSelect(): void
    {

        $q = DB::table('tableName')->build(OperationType::SELECT);
        $this->assertEquals('SELECT * FROM `tableName`', $q);
    }

    public function testSimpleSelectFromAliasedTable(): void
    {
        $q = DB::table('tableName t')->build(OperationType::SELECT);
        $this->assertEquals('SELECT * FROM `tableName` as t', $q);
    }

    public function testFieldsSelect(): void
    {
        $q = DB::table('tableName')->fields('foo','bar','baz')->build(OperationType::SELECT);
        $this->assertEquals('SELECT `tableName`.`foo`, `tableName`.`bar`, `tableName`.`baz` FROM `tableName`', $q);
    }

    public function testRawFieldSelect(): void
    {
        $q = DB::table('tableName')->fields(DB::raw('count(id)'))->build(OperationType::SELECT);
        $this->assertEquals('SELECT count(id) FROM `tableName`', $q);
    }

    public function testWhereEqualsInt(): void
    {
        $q = DB::table('tableName')
            ->fields('foo','bar','baz')
            ->eq('foo',10)
            ->build(OperationType::SELECT);
        $this->assertEquals('SELECT `tableName`.`foo`, `tableName`.`bar`, `tableName`.`baz` FROM `tableName` WHERE `tableName`.`foo` = 10', $q);
    }

    public function testWhereEqualsString(): void
    {
        $q = DB::table('tableName')
            ->fields('foo','bar','baz')
            ->eq('foo','someString')
            ->build(OperationType::SELECT);
        $this->assertEquals('SELECT `tableName`.`foo`, `tableName`.`bar`, `tableName`.`baz` FROM `tableName` WHERE `tableName`.`foo` = "someString"', $q);
    }

    public function testWhereEqualsStringAutoEscaped(): void
    {
        $q = DB::table('tableName')
            ->fields('foo','bar','baz')
            ->eq('foo','some"String"')
            ->build(OperationType::SELECT);
        $this->assertEquals('SELECT `tableName`.`foo`, `tableName`.`bar`, `tableName`.`baz` FROM `tableName` WHERE `tableName`.`foo` = "some\\\"String\\\""', $q);
    }

    public function testWhereNotEquals(): void
    {
        $q = DB::table('tableName')
            ->fields('foo','bar')
            ->ieq('foo',10)
            ->build(OperationType::SELECT);
        $this->assertEquals('SELECT `tableName`.`foo`, `tableName`.`bar` FROM `tableName` WHERE `tableName`.`foo` <> 10', $q);
    }

    public function testWhereGreaterThan(): void
    {
        $q = DB::table('tableName')
            ->fields('foo','bar')
            ->gt('foo',10)
            ->build(OperationType::SELECT);
        $this->assertEquals('SELECT `tableName`.`foo`, `tableName`.`bar` FROM `tableName` WHERE `tableName`.`foo` > 10', $q);
    }

    public function testWhereGreaterThanOrEquals(): void
    {
        $q = DB::table('tableName')
            ->fields('foo','bar')
            ->gte('foo',10)
            ->build(OperationType::SELECT);
        $this->assertEquals('SELECT `tableName`.`foo`, `tableName`.`bar` FROM `tableName` WHERE `tableName`.`foo` >= 10', $q);
    }

    public function testWhereLessThan(): void
    {
        $q = DB::table('tableName')
            ->fields('foo','bar')
            ->lt('foo',10)
            ->build(OperationType::SELECT);
        $this->assertEquals('SELECT `tableName`.`foo`, `tableName`.`bar` FROM `tableName` WHERE `tableName`.`foo` < 10', $q);
    }

    public function testWhereLessThanOrEquals(): void
    {
        $q = DB::table('tableName')
            ->fields('foo','bar')
            ->lte('foo',10)
            ->build(OperationType::SELECT);
        $this->assertEquals('SELECT `tableName`.`foo`, `tableName`.`bar` FROM `tableName` WHERE `tableName`.`foo` <= 10', $q);
    }

    public function testWhereIn(): void
    {
        $q = DB::table('tableName')
            ->fields('foo','bar')
            ->in('foo', [10,20,30])
            ->build(OperationType::SELECT);
        $this->assertEquals('SELECT `tableName`.`foo`, `tableName`.`bar` FROM `tableName` WHERE `tableName`.`foo` IN (10, 20, 30)', $q);
    }

    public function testWhereInWithEmptyArray(): void
    {
        $q = DB::table('tableName')
            ->fields('foo','bar')
            ->in('foo', [])
            ->build(OperationType::SELECT);
        $this->assertEquals('SELECT `tableName`.`foo`, `tableName`.`bar` FROM `tableName` WHERE `tableName`.`foo` IN (select 1 from dual where false)', $q);
    }

    public function testWhereNotIn():void
    {
        $q = DB::table('tableName')
            ->fields('foo','bar')
            ->not()->notIn('foo', ['a','b','c'])
            ->build(OperationType::SELECT);
        $this->assertEquals('SELECT `tableName`.`foo`, `tableName`.`bar` FROM `tableName` WHERE NOT `tableName`.`foo` IN ("a", "b", "c")', $q);
    }

}
