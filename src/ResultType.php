<?php
namespace Fulcrum\Dbal;

class ResultType
{
    const OBJECT = 'OBJECT';
    const ASSOC = 'ASSOC';
    const NUM = 'NUM';
    const INSTANCE = 'INSTANCE';
}
