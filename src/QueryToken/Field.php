<?php
namespace Fulcrum\Dbal\QueryToken;

use Fulcrum\Dbal\Connection;
use Fulcrum\Dbal\Builder\AbstractBuilder;

class Field extends Token {

	protected $field;
	protected $alias;
	/** @var Table */
	protected $table;

	public function __construct($field) {
		$this->field = $field;
		if (is_string($field)) {
            $fieldParts = explode(' ', $field);
            if (count($fieldParts) === 2) {
                $this->field = $fieldParts[0];
                $this->alias = $fieldParts[1];
            } else if (count($fieldParts) === 3 && strtolower($fieldParts[1]) === 'as') {
                $this->field = $fieldParts[0];
                $this->alias = $fieldParts[2];
            }
        }
	}

	public function setTable(Table $table) {
	    $this->table = $table;
    }

	public function render(AbstractBuilder $builder) {
	    $result = '';
	    if (is_a($this->field,Token::class)) {
	        $result = $this->field->render($builder);
        } else {
            if ($this->table) {
                $result = $this->table->renderFieldPrefix() . '.';
            }
            $result .= '`' . $this->field . '`';
            if ($this->alias) {
                $result .= ' as ' . $this->alias;
            }
        }
		return $result;
	}
}
