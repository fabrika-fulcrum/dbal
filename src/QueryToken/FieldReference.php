<?php
namespace Fulcrum\Dbal\QueryToken;

use Fulcrum\Dbal\Builder\AbstractBuilder;
use Fulcrum\Dbal\TableRefContext;

class FieldReference extends Token {

	protected $field;
	protected $table;

	/** @var TableRefContext */
	protected $context;

	public function __construct($field=null) {
	    $parts = explode('.', $field);
	    if (count($parts) == 2) {
	        $this->table = array_shift($parts);
        }
		$this->field = $parts[0];
	}

	public function setContext(?TableRefContext $context) {
	    $this->context = $context;
    }

	public function render(AbstractBuilder $builder) {
	    $tablePart = '';
	    if ($this->table) {
	        $tablePart = '`'.$this->table.'`.';
        } else if ($this->context) {
	        $tablePart = $this->context->_getTable()->renderFieldPrefix().'.';
        }
	    return $tablePart.'`'.$this->field.'`';
	}
}
