<?php
namespace Fulcrum\Dbal\QueryToken;

use Fulcrum\Dbal\Builder\AbstractBuilder;

class GroupsToken extends Token {

	protected $fields = [];

	public function addField($field) {
		if (!is_a($field, Token::class)) {
			$field = new FieldReference($field);
		}
		$this->fields[] = $field;
	}

    public function hasClauses() {
        return count($this->fields) > 0;
    }

	public function render(AbstractBuilder $builder) {
	    if (!$this->hasClauses()) {
	        return null;
        }
		$renderedTokens = [];
		foreach ($this->fields as $field) {
			$renderedTokens[]= $field->render($builder);
		}
		return ($this->fields?'GROUP BY ':'').implode(', ',$renderedTokens);
	}
}
