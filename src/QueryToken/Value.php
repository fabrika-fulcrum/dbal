<?php
namespace Fulcrum\Dbal\QueryToken;

use Fulcrum\Dbal\Builder\AbstractBuilder;

class Value extends Token {
	protected $value;

	public function __construct($value) {
		$this->value = $value;
	}

	public function render(AbstractBuilder $builder) {
		if (is_string($this->value)) {
			return '"'.$builder->escape($this->value).'"';
		}
		if (is_bool($this->value)) {
			return $this->value?1:0;
		}
		if ($this->value === null) {
			return 'NULL';
		}
		return $this->value;
	}
}
