<?php
namespace Fulcrum\Dbal\QueryToken;

use Fulcrum\Dbal\Builder\AbstractBuilder;

class RawToken extends Token {
	protected $contents = '';

	public function __construct($contents) {
		$this->contents = $contents;
	}

	public function render(AbstractBuilder $builder) {
		return $this->contents;
	}
}
