<?php
namespace Fulcrum\Dbal\QueryToken\WhereToken;

use Fulcrum\Dbal\Builder\AbstractBuilder;

class NotToken extends WhereToken {

	public function render(AbstractBuilder $builder) {
		return 'NOT';
	}
}
