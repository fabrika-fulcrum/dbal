<?php
namespace Fulcrum\Dbal\QueryToken\WhereToken;

use Fulcrum\Dbal\Builder\AbstractBuilder;

class RawToken extends WhereToken {
	protected $contents = '';

	public function __construct($contents) {
		$this->contents = $contents;
	}

	public function render(AbstractBuilder $builder) {
		return $this->contents;
	}
}
