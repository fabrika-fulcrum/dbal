<?php
namespace Fulcrum\Dbal\QueryToken\WhereToken;

use Fulcrum\Dbal\Builder\AbstractBuilder;
use Fulcrum\Dbal\QueryToken\FieldReference;
use Fulcrum\Dbal\QueryToken\Token;

class LikeToken extends WhereToken {

	const BEGINS = "BEGINS";
	const ENDS = "ENDS";
	const LIKE = "LIKE";

	protected $field;
	protected $type;
	protected $value;

	public function __construct($field, $value, $type) {
		if (!is_a($field, Token::class)) {
			$field = new FieldReference($field);
		}
		$this->field = $field;
		$this->value = $value;
		$this->type = $type;
	}

	public function render(AbstractBuilder $builder) {
        $this->field->setContext($this->context);
		$value = $builder->escape($this->value);
		switch ($this->type) {
			case static::BEGINS:
				$value .= '%';
				break;
			case static::ENDS:
				$value = '%'.$value;
				break;
			case static::LIKE:
				$value = '%'.$value.'%';
				break;
		}

		return $this->field->render($builder)
			.' LIKE '
			.'"'.$value.'"';
	}
}
