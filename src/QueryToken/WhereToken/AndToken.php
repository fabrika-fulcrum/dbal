<?php
namespace Fulcrum\Dbal\QueryToken\WhereToken;

use Fulcrum\Dbal\Builder\AbstractBuilder;
use Fulcrum\Dbal\Connection;
use Fulcrum\Dbal\Query;

class AndToken extends ConjunctionToken {

	public function render(AbstractBuilder $builder) {
		return 'AND';
	}
}
