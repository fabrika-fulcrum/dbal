<?php
namespace Fulcrum\Dbal\QueryToken\WhereToken;

use Fulcrum\Dbal\QueryToken\Token;
use Fulcrum\Dbal\TableRefContext;

abstract class WhereToken extends Token {

    /** @var TableRefContext */
    protected $context;

    public function setContext(?TableRefContext $context) {
        $this->context = $context;
    }
}
