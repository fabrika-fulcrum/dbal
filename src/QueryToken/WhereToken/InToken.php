<?php
namespace Fulcrum\Dbal\QueryToken\WhereToken;

use Fulcrum\Dbal\Builder\AbstractBuilder;
use Fulcrum\Dbal\QueryToken\FieldReference;
use Fulcrum\Dbal\QueryToken\Token;
use Fulcrum\Dbal\QueryToken\Value;

class InToken extends WhereToken {

	protected $field;
	protected $values = [];

	public function __construct($field, $values) {
		if (!is_a($field, Token::class)) {
			$field = new FieldReference($field);
		}
		$this->field = $field;

		foreach ($values as $value) {
			if (!is_a($value, Token::class)) {
				$value = new Value($value);
			}
			$this->values[] = $value;
		}
	}

	public function render(AbstractBuilder $builder) {
        $this->field->setContext($this->context);
		if (count($this->values) === 0) {
			return $this->field->render($builder)
				.' IN (select 1 from dual where false)';
		}
		$values = [];
		foreach ($this->values as $value) {
			$values[] = $value->render($builder);
		}
		return $this->field->render($builder)
			.' IN ('
			.implode(', ',$values)
			.')';
	}
}
