<?php
namespace Fulcrum\Dbal\QueryToken\WhereToken;

use Fulcrum\Dbal\Builder\AbstractBuilder;
use Fulcrum\Dbal\QueryToken\FieldReference;
use Fulcrum\Dbal\QueryToken\Token;
use Fulcrum\Dbal\QueryToken\Value;

class ComparisonToken extends WhereToken {

    const EQ = "=";
    const IEQ = "<>";
    const GT = ">";
    const GTE = ">=";
    const LT = "<";
    const LTE = "<=";

    protected $field;
    protected $operator;
    protected $value;
	private $hasNullValue = false;

	public function __construct($field, $operator, $value)
    {
    	if (!is_a($field,Token::class )){
    		$field = new FieldReference($field);
		}
        $this->field = $field;
        $this->operator = $operator;
        if ($value === null) {
        	$this->hasNullValue = true;
		}
        if (!is_a($value,Token::class)) {
        	$value = new Value($value);
		}
        $this->value = $value;
    }

	public function render(AbstractBuilder $builder) {
        $this->field->setContext($this->context);
    	$result = $this->field->render($builder);
		if ($this->hasNullValue) {
			if ($this->operator == static::EQ) {
				$result.=' IS NULL';
			} else if ($this->operator == static::IEQ) {
				$result.=' IS NOT NULL';
			} else {
				$result.=$this->operator.' 0';
			}
		} else {
			$result.=' '.$this->operator.' '.$this->value->render($builder);
		}
		return $result;
	}
}
