<?php
namespace Fulcrum\Dbal\QueryToken\WhereToken;

use Fulcrum\Dbal\Builder\AbstractBuilder;
use Fulcrum\Dbal\QueryToken\FieldReference;
use Fulcrum\Dbal\QueryToken\Token;
use Fulcrum\Dbal\QueryToken\Value;

class BetweenToken extends WhereToken {

    protected $field;
    protected $from;
    protected $to;

    public function __construct($field, $from, $to)
    {
		if (!is_a($field, Token::class)) {
			$field = new FieldReference($field);
		}
        $this->field = $field;

		if (!is_a($from,Token::class)) {
			$from = new Value($from);
		}
        $this->from = $from;
		if (!is_a($to,Token::class)) {
			$to = new Value($to);
		}
		$this->to = $to;
    }

	public function render(AbstractBuilder $builder) {
        $this->field->setContext($this->context);
        return $builder->implodeParts(
            [
                $this->field->render($builder),
                'BETWEEN',
                $this->from->render($builder),
                'AND',
                $this->to->render($builder)
            ]
        );
	}
}
