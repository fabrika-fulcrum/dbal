<?php
namespace Fulcrum\Dbal\QueryToken\WhereToken;

use Fulcrum\Dbal\Builder\AbstractBuilder;
use Fulcrum\Dbal\Connection;
use Fulcrum\Dbal\Query;

class OrToken extends ConjunctionToken {

	public function render(AbstractBuilder $builder) {
		return 'OR';
	}
}
