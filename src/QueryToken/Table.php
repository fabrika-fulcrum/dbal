<?php
namespace Fulcrum\Dbal\QueryToken;

use Fulcrum\Dbal\Builder\AbstractBuilder;

class Table extends Token {

	protected $name;
	protected $alias;

	public function __construct($name, $alias='') {
	    $this->setName($name, $alias);
	}

	public function setName($name, $alias='') {
	    if ($alias) {
	        $this->setName($name);
	        $this->alias = $alias;
        }
        if (is_string($name)) {
            $name = str_replace(' as ', ' ', $name);
            $this->name = $name;
            $nameParts = explode(' ', $name);
            if (count($nameParts) === 2) {
                $this->name = $nameParts[0];
                $this->alias = $nameParts[1];
            } else if (count($nameParts) === 3 && strtolower($nameParts[1]) === 'as') {
                $this->name = $nameParts[0];
                $this->alias = $nameParts[2];
            }
        } else if (is_a($name, Token::class)) {
	        $this->name = $name;
        }

    }

/*    public function getName() {
	    return ''.$this->name.($this->alias?' '.$this->alias:'');
    }*/

	public function renderFieldPrefix() {
		return '`'.($this->alias?$this->alias:$this->name).'`';
	}

	public function render(AbstractBuilder $builder) {
	    if (is_a($this->name, Token::class)) {
	        $name = $this->name->render($builder);
        } else {
	        $name = '`'.$this->name.'`';
        }
		return $name.($this->alias?' as '.$this->alias:'');
	}
}
