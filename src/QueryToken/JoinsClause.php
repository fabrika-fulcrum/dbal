<?php
namespace Fulcrum\Dbal\QueryToken;

use Fulcrum\Dbal\Builder\AbstractBuilder;

class JoinsClause extends Token {

	protected $joins = [];

	public function addJoin($join){
		$this->joins[] = $join;
	}

	public function hasClauses() {
		return count($this->joins) > 0;
	}

	public function render(AbstractBuilder $builder) {
	    if (!$this->hasClauses()) {
	        return null;
        }
		$renderedTokens = [];
		foreach ($this->joins as $join) {
			$renderedTokens[] = $join->render($builder);
		}
		return implode(' ',$renderedTokens);
	}
}
