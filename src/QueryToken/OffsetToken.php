<?php
namespace Fulcrum\Dbal\QueryToken;

use Fulcrum\Dbal\Builder\AbstractBuilder;

class OffsetToken extends Token {

	protected $skip;
	protected $take;

	const MAX_LIMIT = 999999999;

	public function skip($skip){
		$this->skip = $skip;
	}

	public function take($take){
		$this->take = $take;
	}

    public function hasClauses() {
        return ($this->skip !== null || $this->take !== null);
    }

	public function render(AbstractBuilder $builder) {
	    if (!$this->hasClauses()) {
            return null;
        }
		$result = '';
		if ($this->skip !== null && $this->take !==null) {
			$result = 'LIMIT '.$this->skip.', '.$this->take;
		} else if ($this->skip === null && $this->take !==null) {
			$result = 'LIMIT '.$this->take;
		} else if ($this->skip !== null && $this->take === null) {
			$result = 'LIMIT '.$this->skip.', '.static::MAX_LIMIT;
		}
		return $result;
	}
}
