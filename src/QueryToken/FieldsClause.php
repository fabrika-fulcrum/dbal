<?php
namespace Fulcrum\Dbal\QueryToken;

use Fulcrum\Dbal\Builder\AbstractBuilder;

class FieldsClause extends Token {

	protected $fields = [];

	public function addField($field, $table=null){
		if (is_a($field, Field::class)) {
			$this->fields[] = $field;
		} else {
			$newField = new Field($field);
			if ($table !== null) {
				$newField->setTable($table);
			}
			$this->fields[] = $newField;
		}
	}

	public function render(AbstractBuilder $builder) {
		if (count($this->fields) == 0){
			return '*';
		}
		$result = [];
		foreach ($this->fields as $field) {
			$result[] = $field->render($builder);
		}
		return implode(', ',$result);
	}
}
