<?php
namespace Fulcrum\Dbal\QueryToken;

use Fulcrum\Dbal\Builder\AbstractBuilder;

class OrdersToken extends Token {

	/** @var OrderToken[] */
	protected $orders = [];

	public function addToken(OrderToken $token) {
		$this->orders[] = $token;
	}

	public function hasClauses() {
	    return count($this->orders) > 0;
    }

	public function render(AbstractBuilder $builder) {
	    if (!$this->hasClauses()) {
	        return null;
        }
		$renderedTokens = [];
		foreach ($this->orders as $order) {
			$renderedTokens[]= $order->render($builder);
		}
		return ($this->orders?'ORDER BY ':'').implode(', ',$renderedTokens);
	}
}
