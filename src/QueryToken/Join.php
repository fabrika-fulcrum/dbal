<?php
namespace Fulcrum\Dbal\QueryToken;

use Fulcrum\Dbal\Builder\AbstractBuilder;

class Join extends Token {

	const JOIN="INNER JOIN";
	const LEFT="LEFT JOIN";
	const RIGHT="RIGHT JOIN";
	const OUTER="OUTER JOIN";

    /** @var Table */
	protected $parentTable;

	protected $type;
	/** @var Table */
	protected $table;
	/** @var FieldReference */
	protected $field;
    /** @var FieldReference */
	protected $joinedField;

    /** @var OnClause */
	public $where;

	public function __construct($type, $table, $field, $joinedField) {
		$this->type = $type;
		$this->table = is_a($table,Table::class)?$table:new Table($table);
		$this->field = is_a($field,FieldReference::class)?$field:new FieldReference($field);
		$this->joinedField = is_a($joinedField,FieldReference::class)?$joinedField:new FieldReference($joinedField);
		$this->where = new OnClause();
	}

	public function setParentTable($parentTable) {
		$this->parentTable = $parentTable;
	}

	public function getType() {
	    return $this->type;
    }

    public function getTable(){
	    return $this->table;
    }

    public function getField(){
		return $this->field;
    }

    public function getJoinedField() {
    	return $this->joinedField;
	}

	public function render(AbstractBuilder $builder) {
	    return $builder->implodeParts([
            $this->type,
            $this->table->render($builder),
            'ON',
            $this->table->renderFieldPrefix().'.'.$this->field->render($builder),
            '=',
            $this->parentTable->renderFieldPrefix().'.'.$this->joinedField->render($builder),
            $this->where->render($builder)
        ]);
	}
}
