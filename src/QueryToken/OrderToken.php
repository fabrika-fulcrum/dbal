<?php
namespace Fulcrum\Dbal\QueryToken;

use Fulcrum\Dbal\Builder\AbstractBuilder;

class OrderToken extends Token {

	const ASC = "ASC";
	const DESC = "DESC";

	/** @var FieldReference */
	protected $field;
	protected $direction;

	public function __construct($field, $direction) {
		if (!is_a($field, Token::class)) {
			$field = new FieldReference($field);
		}
		$this->field = $field;
		$this->direction = $direction;
	}

	public function render(AbstractBuilder $builder) {
		return $this->field->render($builder)
			.' '
			.$this->direction;
	}
}
