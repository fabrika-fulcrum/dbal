<?php
namespace Fulcrum\Dbal\QueryToken;

use Fulcrum\Dbal\Builder\AbstractBuilder;
use Fulcrum\Dbal\QueryToken\WhereToken\AndToken;
use Fulcrum\Dbal\QueryToken\WhereToken\ConjunctionToken;
use Fulcrum\Dbal\QueryToken\WhereToken\NotToken;
use Fulcrum\Dbal\QueryToken\WhereToken\WhereToken;
use Fulcrum\Dbal\TableRefContext;

class WhereClause extends WhereToken {

	/** @var WhereToken[] */
	protected $clauses = [];

	protected $isInner;

	/** @var TableRefContext */
	protected $context;

	public function __construct($isInner = false) {
		$this->isInner = $isInner;
	}

	public function setContext(?TableRefContext $context) {
	    $this->context = $context;
    }

	public function addToken(WhereToken $token) {
		if (count($this->clauses) > 0
			&& !is_a($token, ConjunctionToken::class)
			&& (count($this->clauses) > 0 && (
			    !is_a($this->clauses[count($this->clauses)-1], ConjunctionToken::class)
                && !is_a($this->clauses[count($this->clauses)-1], NotToken::class)
                )
            )
		) {
			$this->clauses[] = new AndToken();
		}
		$token->setContext($this->context);
		$this->clauses[] = $token;
	}

	public function hasClauses(){
		return count($this->clauses) > 0;
	}

	protected function renderClauses(AbstractBuilder $builder) {
        $renderedTokens = [];
        foreach ($this->clauses as $clause) {
            $renderedTokens[] = $clause->render($builder, $this->context);
        }
        return implode(' ', $renderedTokens);
    }

	public function render(AbstractBuilder $builder) {
	    if (!$this->hasClauses()) {
	        return null;
        }
		$renderedParts = [
            $this->isInner?'(':'WHERE ',
            $this->renderClauses($builder),
            $this->isInner?')':''
        ];
		return implode('',$renderedParts);
	}
}
