<?php
namespace Fulcrum\Dbal\QueryToken;

use Fulcrum\Dbal\Builder\AbstractBuilder;

abstract class Token {

	abstract public function render(AbstractBuilder $builder);
}
