<?php
namespace Fulcrum\Dbal\QueryToken;

use Fulcrum\Dbal\Builder\AbstractBuilder;
use Fulcrum\Dbal\QueryToken\WhereToken\AndToken;
use Fulcrum\Dbal\QueryToken\WhereToken\ConjunctionToken;
use Fulcrum\Dbal\QueryToken\WhereToken\NotToken;
use Fulcrum\Dbal\QueryToken\WhereToken\WhereToken;
use Fulcrum\Dbal\TableRefContext;

class OnClause extends WhereClause {

	public function render(AbstractBuilder $builder) {
        if (!$this->hasClauses()) {
            return null;
        }
        $renderedParts = [
            $this->isInner?'(':'AND ',
            $this->renderClauses($builder),
            $this->isInner?')':''
        ];
        return implode('',$renderedParts);
	}
}
