<?php
namespace Fulcrum\Dbal\QueryToken;

use Fulcrum\Dbal\Builder\AbstractBuilder;
use Fulcrum\Dbal\TableRefContext;

class PrimaryKey extends FieldReference {

    /** @var TableRefContext */
    protected $context;

	public function render(AbstractBuilder $builder) {
		// TODO: Implement way to find real primary key
        $this->field = 'id';
		return parent::render($builder);
	}

	public function setContext(?TableRefContext $context) {
	    $this->context = $context;
    }
}
