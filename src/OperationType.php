<?php
namespace Fulcrum\Dbal;

class OperationType {
	const QUERY = "QUERY";
	const SELECT = "SELECT";
	const INSERT = "INSERT";
	const UPDATE = "UPDATE";
	const DELETE = "DELETE";
}
