<?php
namespace Fulcrum\Dbal;

use Fulcrum\Dbal\Exception\ConnectionException;
use Fulcrum\Dbal\Exception\DbalException;

interface ConnectionInterface {

	public function __construct(string $database, string $username, string $password, string $host);

	public function escape($string);

	public function run($query);

	public function error();

	public function insert_id();

	public function affected_rows();

	public function query_time();

	public function get($query, $method);

	public function getRow($query, $method);

    public function getVar($query);

    public function getColumn($query, $column=null);

}
