<?php

namespace Fulcrum\Dbal;

class Result implements \Iterator, \ArrayAccess, \JsonSerializable, \Countable
{

    protected $container = [];
    protected $position = 0;

    protected $type;

    public function __construct($records, $type)
    {
        $this->type = $type;
        $this->container = $records;
    }

    public function offsetSet($offset, $value)
    {
        if ($offset === null) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    public function current()
    {
        return $this->container[$this->position];
    }

    public function next()
    {
        ++$this->position;
    }

    public function key()
    {
        return $this->position;
    }

    public function valid()
    {
        return isset($this->container[$this->position]);
    }

    public function rewind()
    {
        $this->position = 0;
    }

    public function count()
    {
        return \count($this->container);
    }

    public function jsonSerialize()
    {
        return $this->container;
    }

    public function __toArray()
    {
        return $this->container;
    }

    public function mapBy($fieldName)
    {
        $result = [];
        foreach ($this->container as $item) {
            if ($this->type == ResultType::OBJECT) {
                $result[$item->{$fieldName}] = $item;
            } else {
                $result[$item[$fieldName]] = $item;
            }
        }
        return $result;
    }

    public function mapPairs($keyField, $valueField)
    {
        $result = [];
        foreach ($this->container as $item) {
            if ($this->type === ResultType::OBJECT) {
                $result[$item->{$keyField}] = $item->{$valueField};
            } else {
                $result[$item[$keyField]] = $item[$valueField];
            }
        }
        return $result;
    }

    public function getColumn($fieldName)
    {
        $result = [];
        foreach ($this->container as $item) {
            if ($this->type == ResultType::OBJECT) {
                $result[] = $item->{$fieldName};
            } else {
                $result[] = $item[$fieldName];
            }
        }
        return $result;
    }

    public function getColumnDistinct($fieldName)
    {
        return array_unique($this->getColumn($fieldName));
    }
}
