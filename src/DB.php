<?php
namespace Fulcrum\Dbal;

use Fulcrum\Dbal\QueryToken\RawToken;

/**
 * Class DB
 * @package Dbal
 *
 * Static class to initiate access to most frequent Database operations
 * This package is focused on MySql and will not explicitly support other databases
 */
class DB {

	protected static $connection;

	public static function setConnection($connection) {
		static::$connection = $connection;
	}

	public static function table($table, $alias='') {
		$query = new Query(static::$connection);
		$query->table($table, $alias);
		return $query;
	}

	public static function raw($value) {
		return new RawToken($value);
	}

	public static function run($query) {
        return static::$connection->run($query);
	}
}
