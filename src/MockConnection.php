<?php

namespace Fulcrum\Dbal;

use Fulcrum\Dbal\Exception\ConnectionException;
use Fulcrum\Dbal\Exception\DbalException;

class MockConnection implements ConnectionInterface
{

    public function __construct($database, $username, $password, $host = 'localhost')
    {

    }

    public function escape($string)
    {
        // Perform a very basic, unsafe escape
        return str_replace(['"', "\\"], ["\\\"", "\\\\"], $string);
    }

    public function run($query)
    {
        return [];
    }

    public function error()
    {
        return '';
    }

    public function insert_id()
    {
        return 1;
    }

    public function affected_rows()
    {
        return 1;
    }

    public function query_time()
    {

    }

    public function get($query, $method)
    {
        return [];
    }

    public function getRow($query, $method)
    {
        return [];
    }

    public function getVar($query)
    {
        return 1;
    }

    public function getColumn($query, $column = null)
    {
        return [];
    }

}
