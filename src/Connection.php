<?php
namespace Fulcrum\Dbal;

use Fulcrum\Dbal\Exception\ConnectionException;
use Fulcrum\Dbal\Exception\DbalException;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

class Connection implements ConnectionInterface {

    /** @var \mysqli */
	protected $mysqli;
	protected $insert_id;

	protected $logger;

	public function __construct(string $database, string $username, string $password, string $host) {
	    $this->logger = new NullLogger();

		$this->mysqli = new \mysqli($host, $username,$password,$database);
		if ($this->mysqli->connect_error) {
            throw new ConnectionException($this->mysqli->connect_error);
		}
        $this->mysqli->set_charset('utf8mb4');
        $this->mysqli->query('set session sql_mode=""');
    }

    public function setLogger(LoggerInterface $logger) {
	    $this->logger = $logger;
    }

	public function escape($string) {
		return $this->mysqli->real_escape_string($string);
	}

	public function run($query) {
        $this->logger->debug($query);
	    $result = $this->mysqli->query($query);
	    if ($result === false) {
	        $error_message = 'Mysql error '.$this->error();
	        $this->logger->error($error_message);
	        throw new ConnectionException($error_message);
        }
	    return $result;
	}

	public function error()
	{
		return $this->mysqli->error;
	}

	public function insert_id()
	{
		return $this->mysqli->insert_id;
	}

	public function affected_rows(){
		return $this->mysqli->affected_rows;
	}

	public function query_time(){

	}

	public function get($query, $method, $className = '', $params = []) {
        $result = new Result([], ResultType::OBJECT);
        $queryResults = $this->run($query);
        if ($queryResults && $queryResults->num_rows > 0) {
            switch ($method) {
                case ResultType::INSTANCE:
                    $result = new Result([], ResultType::OBJECT);
                    $useRecordMethod = false;
                    try {
                        $reflection = new \ReflectionClass($className);
                        if ($reflection->getMethod('fromRecord')->isStatic()) {
                            $useRecordMethod = true;
                        }
                    } catch (\Exception $e) {

                    }
                    if ($useRecordMethod) {
                        while ($row = $queryResults->fetch_object()) {
                            $result[] = $className::fromRecord($row, ...$params);
                        }
                    } else {
                        while ($instance = $queryResults->fetch_object($className, $params)) {
                            $result[] = $instance;
                        }
                    }
                    break;
                case ResultType::OBJECT:
                    $result = new Result([], ResultType::OBJECT);
                    while ($row = $queryResults->fetch_object()) {
                        $result[] = $row;
                    }
                    break;
                case ResultType::ASSOC:
                    $result = new Result([], ResultType::ASSOC);
                    while ($row = $queryResults->fetch_assoc()) {
                        $result[] = $row;
                    }
                    break;
                case ResultType::NUM:
                    $result = new Result([], ResultType::NUM);
                    while ($row = $queryResults->fetch_array(MYSQLI_NUM)) {
                        $result[] = $row;
                    }
                    break;
            }
            $queryResults->free();
        }
        return $result;
	}

	public function getRow($query, $method, $className = '', $params = [])
    {
        $result = null;
        $queryResults = $this->run($query);
        if ($queryResults->num_rows > 0) {
            switch ($method) {
                case ResultType::INSTANCE:
                    $useRecordMethod = false;
                    try {
                        $reflection = new \ReflectionClass($className);
                        if ($reflection->getMethod('fromRecord')->isStatic()) {
                            $useRecordMethod = true;
                        }
                    } catch (\Exception $e) {

                    }
                    if ($useRecordMethod) {
                        $row = $queryResults->fetch_object();
                        $result = $className::fromRecord($row, ...$params);
                    } else {
                        $instance = $queryResults->fetch_object($className, $params);
                        $result = $instance;
                    }
                    break;
                case ResultType::OBJECT:
                    $result = $queryResults->fetch_object();
                    break;
                case ResultType::ASSOC:
                    $result = $queryResults->fetch_assoc();
                    break;
                case ResultType::NUM:
                    $result = $queryResults->fetch_array(MYSQLI_NUM);
                    break;
            }
            $queryResults->free();
        }
        return $result;
    }

    public function getVar($query) {
        $result = null;
        $queryResults = $this->run($query);
        if ($queryResults->num_rows > 0) {
            $result = $queryResults->fetch_array(MYSQLI_NUM)[0];
            $queryResults->free();
        }
        return $result;
    }

    public function getColumn($query, $column=null){
        $result = [];
        $queryResults = $this->run($query);
        if ($queryResults->num_rows > 0) {
            while ($row = $queryResults->fetch_array(MYSQLI_NUM)) {
                $result[] = $row[$column===null?0:$column];
            }
        }
        $queryResults->free();
        return $result;
    }

}
