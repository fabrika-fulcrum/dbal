<?php

namespace Fulcrum\Dbal;

use Fulcrum\Dbal\Builder\DeleteBuilder;
use Fulcrum\Dbal\Builder\InsertBuilder;
use Fulcrum\Dbal\Builder\NullBuilder;
use Fulcrum\Dbal\Builder\SelectBuilder;
use Fulcrum\Dbal\Builder\UpdateBuilder;
use Fulcrum\Dbal\QueryState\JoiningState;
use Fulcrum\Dbal\QueryState\QueryState;
use Fulcrum\Dbal\QueryState\WhereState;
use Fulcrum\Dbal\QueryToken\FieldsClause;
use Fulcrum\Dbal\QueryToken\GroupsToken;
use Fulcrum\Dbal\QueryToken\Join;
use Fulcrum\Dbal\QueryToken\JoinsClause;
use Fulcrum\Dbal\QueryToken\OffsetToken;
use Fulcrum\Dbal\QueryToken\OrdersToken;
use Fulcrum\Dbal\QueryToken\OrderToken;
use Fulcrum\Dbal\QueryToken\PrimaryKey;
use Fulcrum\Dbal\QueryToken\Table;
use Fulcrum\Dbal\QueryToken\WhereClause;
use Fulcrum\Dbal\QueryToken\WhereToken\BetweenToken;
use Fulcrum\Dbal\QueryToken\WhereToken\ComparisonToken;
use Fulcrum\Dbal\QueryToken\WhereToken\InToken;
use Fulcrum\Dbal\QueryToken\WhereToken\LikeToken;
use Fulcrum\Dbal\QueryToken\WhereToken\NotToken;
use Fulcrum\Dbal\QueryToken\WhereToken\OrToken;
use Fulcrum\Dbal\QueryToken\WhereToken\RawToken;

class Query implements TableRefContext, QueryState
{

    /** @var  Connection */
    protected $connection;

    protected $resultType;
    protected $resultClass = '';
    protected $instanceParams = [];

    protected $operationType;

    /** @var Table */
    protected $table;

    /** @var FieldsClause */
    protected $fields = [];

    protected $values = [];

    /** @var JoinsClause */
    protected $joins;

    /** @var WhereClause */
    protected $where;

    /** @var OrdersToken */
    protected $order;

    /** @var  GroupsToken */
    protected $groups;

    /** @var OffsetToken */
    protected $offset;

    public function __construct(ConnectionInterface $connection)
    {
        $this->connection = $connection;
        $this->operationType = OperationType::QUERY;
        $this->resultType = ResultType::OBJECT;
        $this->fields = new FieldsClause();
        $this->joins = new JoinsClause();
        $this->where = new WhereClause();
        $this->where->setContext($this);
        $this->order = new OrdersToken();
        $this->groups = new GroupsToken();
        $this->offset = new OffsetToken();
    }

    public function table($table, $alias = '')
    {
        if ($this->table === null) {
            $this->table = new Table($table, $alias);
        } else {
            $this->table->setName($table, $alias);
        }
        return $this;
    }

    public function fields(...$fields)
    {
        if (count($fields) === 1 && is_array($fields[0])) {
            $fields = $fields[0];
        }
        foreach ($fields as $field) {
            $this->fields->addField($field, $this->table);
        }
        return $this;
    }

    protected function values($values)
    {
        $this->values = $values;
    }


    /* --- Joins --- */

    public function join($joinTable, $joinField, $field)
    {
        return $this->addJoin(Join::JOIN, $joinTable, $joinField, $field);
    }

    public function leftJoin($joinTable, $joinField, $field)
    {
        return $this->addJoin(Join::LEFT, $joinTable, $joinField, $field);
    }

    public function rightJoin($joinTable, $joinField, $field)
    {
        return $this->addJoin(Join::RIGHT, $joinTable, $joinField, $field);
    }

    public function innerJoin($joinTable, $joinField, $field)
    {
        return $this->addJoin(Join::JOIN, $joinTable, $joinField, $field);
    }

    public function outerJoin($joinTable, $joinField, $field)
    {
        return $this->addJoin(Join::OUTER, $joinTable, $joinField, $field);
    }

    protected function addJoin($joinType, $joinTable, $joinField, $field)
    {
        $join = new Join($joinType, $joinTable, $joinField, $field);
        $join->setParentTable($this->table);
        $this->joins->addJoin($join);
        return new JoiningState($this, $this, $join, $this->joins);
    }

    /* --- Wheres --- */

    public function id($value)
    {
        return $this->eq(new PrimaryKey(), $value);
    }

    public function eq($field, $value)
    {
        return $this->addComparison($field, ComparisonToken::EQ, $value);
    }

    public function ieq($field, $value)
    {
        return $this->addComparison($field, ComparisonToken::IEQ, $value);
    }

    public function gt($field, $value)
    {
        return $this->addComparison($field, ComparisonToken::GT, $value);
    }

    public function gte($field, $value)
    {
        return $this->addComparison($field, ComparisonToken::GTE, $value);
    }

    public function lt($field, $value)
    {
        return $this->addComparison($field, ComparisonToken::LT, $value);
    }

    public function lte($field, $value)
    {
        return $this->addComparison($field, ComparisonToken::LTE, $value);
    }

    public function isNull($field)
    {
        return $this->addComparison($field, ComparisonToken::EQ, null);
    }

    public function isNotNull($field)
    {
        return $this->addComparison($field, ComparisonToken::IEQ, null);
    }

    protected function addComparison($field, $operation, $value)
    {
        $this->where->addToken(new ComparisonToken($field, $operation, $value));
        return $this;
    }

    public function in($field, $values)
    {
        $this->where->addToken(new InToken($field, $values));
        return $this;
    }



    public function between($field, $from, $to)
    {
        $this->where->addToken(new BetweenToken($field, $from, $to));
        return $this;
    }

    public function like($field, $value)
    {
        $this->addLikeCondition($field, $value, LikeToken::LIKE);
        return $this;
    }

    public function contains($field, $value)
    {
        return $this->like($field, $value);
    }

    public function begins($field, $value)
    {
        $this->addLikeCondition($field, $value, LikeToken::BEGINS);
        return $this;
    }

    public function ends($field, $value)
    {
        $this->addLikeCondition($field, $value, LikeToken::ENDS);
        return $this;
    }

    protected function addLikeCondition($field, $value, $type)
    {
        $this->where->addToken(new LikeToken($field, $value, $type));
    }

    public function where()
    {
        $whereClause = new WhereClause(false);
        $this->where->addToken($whereClause);
        $whereState = new WhereState($this, $this, $whereClause);
        return $whereState;
    }

    public function or()
    {
        $this->where->addToken(new OrToken());
        return $this;
    }

    public function not()
    {
        $this->where->addToken(new NotToken());
        return $this;
    }

    public function whereRaw($rawValue)
    {
        $this->where->addToken(new RawToken($rawValue));
        return $this;
    }

    /* --- Order by ---*/

    public function asc($field)
    {
        $this->order->addToken(new OrderToken($field, OrderToken::ASC));
        return $this;
    }

    public function desc($field)
    {
        $this->order->addToken(new OrderToken($field, OrderToken::DESC));
        return $this;
    }

    /* --- Group by ---*/

    public function groupBy(...$fields)
    {
        foreach ($fields as $field) {
            $this->groups->addField($field);
        }
        return $this;
    }

    /* -- Limit -- */

    public function limit($amount)
    {
        return $this->take($amount);
    }

    public function offset($amount)
    {
        return $this->skip($amount);
    }

    public function skip($amount)
    {
        $this->offset->skip($amount);
        return $this;
    }

    public function take($amount)
    {
        $this->offset->take($amount);
        return $this;
    }

    /* --- Build ---- */

    public function build($type = null, $params = null)
    {
        if ($type !== null) {
            $this->operationType = $type;
            if ($params !== null) {
                switch ($this->operationType) {
                    case OperationType::INSERT:
                    case OperationType::UPDATE:
                        $this->values($params);
                        break;
                    case OperationType::DELETE:
                        $this->id($params);
                        break;
                }
            }
        }
        switch ($this->operationType) {
            case OperationType::SELECT:
                $builder = new SelectBuilder(
                    $this->connection,
                    $this->table,
                    $this->fields,
                    $this->joins,
                    $this->where,
                    $this->order,
                    $this->groups,
                    $this->offset
                );
                break;
            case OperationType::INSERT:
                $builder = new InsertBuilder(
                    $this->connection,
                    $this->table,
                    $this->values
                );
                break;
            case OperationType::UPDATE:
                $builder = new UpdateBuilder(
                    $this->connection,
                    $this->table,
                    $this->values,
                    $this->where,
                    $this->order,
                    $this->offset
                );
                break;
            case OperationType::DELETE:
                $builder = new DeleteBuilder(
                    $this->connection,
                    $this->table,
                    $this->where,
                    $this->order,
                    $this->offset
                );
                break;
            default:
                $builder = new NullBuilder($this->connection);
        }
        return $builder->build();
    }

    /*-- Context -- */

    public function _getTable(): Table
    {
        return $this->table;
    }

    /*--- Output ---*/

    public function asClass($className, $params=[])
    {
        $this->resultType = ResultType::INSTANCE;
        $this->resultClass = $className;
        $this->instanceParams = $params;
        return $this;
    }

    public function asArray()
    {
        $this->resultType = ResultType::ASSOC;
        return $this;
    }

    public function asNumericArray()
    {
        $this->resultType = ResultType::NUM;
        return $this;
    }

    /**
     * return multiple row results
     */
    public function get()
    {
        $this->operationType = OperationType::SELECT;
        return $this->connection->get($this->build(), $this->resultType, $this->resultClass, $this->instanceParams);
    }

    /**
     * return single row result
     */
    public function getRow()
    {
        $this->operationType = OperationType::SELECT;
        return $this->connection->getRow($this->build(), $this->resultType, $this->resultClass, $this->instanceParams);
    }

    /**
     * return single variable
     */
    public function getVar()
    {
        $this->operationType = OperationType::SELECT;
        return $this->connection->getVar($this->build());
    }

    /**
     * return single column
     * use column parameter to "pluck" otherwise get just the first column of the result
     */
    public function getColumn($column = null)
    {
        $this->operationType = OperationType::SELECT;
        $records = $this->connection->get($this->build(), ResultType::ASSOC);
        $result = [];
        foreach ($records as $record) {
            $result[] = $record[$column];
        }
        return $result;
    }

    /**
     * insert data in given table
     * return last insert id
     * @param $data
     */
    public function insert($data)
    {
        $this->values($data);
        $this->operationType = OperationType::INSERT;
        $this->connection->run($this->build());
        return $this->connection->insert_id();
    }

    /**
     * update
     * return number of affected rows
     */
    public function update($data)
    {
        $this->values($data);
        $this->operationType = OperationType::UPDATE;
        $this->connection->run($this->build());
        return $this->connection->affected_rows();
    }

    /**
     * delete
     * return number of affected rows
     */
    public function delete($pk = null)
    {
        if ($pk !== null) {
            $this->id($pk);
        }
        $this->operationType = OperationType::DELETE;
        $this->connection->run($this->build());
        return $this->connection->affected_rows();
    }

}
