<?php
namespace Fulcrum\Dbal\QueryState;

use Fulcrum\Dbal\Query;
use Fulcrum\Dbal\QueryToken\Field;
use Fulcrum\Dbal\QueryToken\Join;
use Fulcrum\Dbal\QueryToken\JoinsClause;
use Fulcrum\Dbal\QueryToken\OnClause;
use Fulcrum\Dbal\QueryToken\PrimaryKey;
use Fulcrum\Dbal\QueryToken\Table;
use Fulcrum\Dbal\QueryToken\WhereClause;
use Fulcrum\Dbal\QueryToken\WhereToken\BetweenToken;
use Fulcrum\Dbal\QueryToken\WhereToken\ComparisonToken;
use Fulcrum\Dbal\QueryToken\WhereToken\InToken;
use Fulcrum\Dbal\QueryToken\WhereToken\LikeToken;
use Fulcrum\Dbal\QueryToken\WhereToken\NotToken;
use Fulcrum\Dbal\QueryToken\WhereToken\OrToken;
use Fulcrum\Dbal\QueryToken\WhereToken\RawToken;
use Fulcrum\Dbal\TableRefContext;

class JoiningState implements AbstractState, TableRefContext {

    /** @var  Query|JoiningState */
    protected $parentState;

    /** @var Join */
    protected $joinToken;

    /** @var JoinsClause */
	protected $joins;


    public function __construct(QueryState $parentState, ?TableRefContext $context, Join $joinToken, JoinsClause $joinsClause) {
        $this->parentState = $parentState;
        $this->joinToken = $joinToken;
        $this->joinToken->where->setContext($this);
        $this->joins = $joinsClause;
    }

    public function fields(...$fields) {
        if (count($fields) === 1 && is_array($fields[0])) {
            $fields = $fields[0];
        }
        foreach ($fields as $field) {
        	if (!is_a($field, Field::class)) {
				$newField = new Field($field);
				$newField->setTable($this->joinToken->getTable());
				$this->parentState->fields($newField);
			} else {
        		$this->parentState->fields($field);
			}
        }
        return $this;
    }

    public function back()
    {
        return $this->parentState;
    }

    public function end() {
        if ($this->parentState && is_a($this->parentState, AbstractState::class)) {
            return $this->parentState->end();
        }
        return $this->parentState;
    }

	/* --- Joins --- */

	public function join($joinTable, $joinField, $field) {
		return $this->addJoin(Join::JOIN, $joinTable, $joinField, $field);
	}

	public function leftJoin($joinTable, $joinField, $field) {
		return $this->addJoin(Join::LEFT, $joinTable, $joinField, $field);
	}

	public function rightJoin($joinTable, $joinField, $field) {
		return $this->addJoin(Join::RIGHT, $joinTable, $joinField, $field);
	}

    public function innerJoin($joinTable, $joinField, $field) {
        return $this->addJoin(Join::JOIN, $joinTable, $joinField, $field);
    }

	public function outerJoin($joinTable, $joinField, $field) {
		return $this->addJoin(Join::OUTER, $joinTable, $joinField, $field);
	}

	protected function addJoin($joinType, $joinTable, $joinField, $field) {
		$join = new Join($joinType, $joinTable, $joinField, $field);
		$join->setParentTable($this->joinToken->getTable());
		$this->joins->addJoin($join);
		return new JoiningState($this, $this, $join, $this->joins);
	}

	/* --- Wheres --- */

    public function id($value) {
        return $this->eq(new PrimaryKey(), $value);
    }
    public function eq($field, $value) {
        return $this->addComparison($field, ComparisonToken::EQ, $value);
    }
    public function ieq($field, $value) {
        return $this->addComparison($field, ComparisonToken::IEQ, $value);
    }
    public function gt($field, $value) {
        return $this->addComparison($field, ComparisonToken::GT, $value);
    }
    public function gte($field, $value) {
        return $this->addComparison($field, ComparisonToken::GTE, $value);
    }
    public function lt($field, $value) {
        return $this->addComparison($field, ComparisonToken::LT, $value);
    }
    public function lte($field, $value) {
        return $this->addComparison($field, ComparisonToken::LTE, $value);
    }
    public function isNull($field) {
        return $this->addComparison($field, ComparisonToken::EQ, null);
    }
    public function isNotNull($field) {
        return $this->addComparison($field, ComparisonToken::IEQ, null);
    }

    protected function addComparison($field, $operation, $value) {
        $this->joinToken->where->addToken(new ComparisonToken($field, $operation, $value));
        return $this;
    }

    public function in($field, $values) {
        $this->joinToken->where->addToken(new InToken($field, $values));
        return $this;
    }
    public function between($field, $from, $to) {
        $this->joinToken->where->addToken(new BetweenToken($field,$from,$to));
        return $this;
    }

    public function like($field, $value) {
        $this->addLikeCondition($field, $value, LikeToken::LIKE);
        return $this;
    }
    public function contains($field, $value) {
        return $this->like($field, $value);
    }
    public function begins($field, $value) {
        $this->addLikeCondition($field, $value, LikeToken::BEGINS);
        return $this;
    }
    public function ends($field, $value) {
        $this->addLikeCondition($field, $value, LikeToken::ENDS);
        return $this;
    }
    protected function addLikeCondition($field, $value, $type){
        $this->joinToken->where->addToken(new LikeToken($field,$value, $type));
    }

    public function where() {
        $whereClause = new OnClause(true);
        $this->joinToken->where->addToken($whereClause);
        $whereState = new WhereState($this, $this, $whereClause);
        return $whereState;
    }

    public function or() {
        $this->joinToken->where->addToken(new OrToken());
        return $this;
    }
    public function not() {
        $this->joinToken->where->addToken(new NotToken());
        return $this;
    }
    public function whereRaw($rawValue) {
        $this->joinToken->where->addToken(new RawToken($rawValue));
        return $this;
    }

    /* --- Context --- */


    public function _getTable(): Table
    {
        return $this->joinToken->getTable();
    }
}
