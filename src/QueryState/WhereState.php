<?php
namespace Fulcrum\Dbal\QueryState;

use Fulcrum\Dbal\Query;
use Fulcrum\Dbal\QueryToken\PrimaryKey;
use Fulcrum\Dbal\QueryToken\WhereClause;
use Fulcrum\Dbal\QueryToken\WhereToken\BetweenToken;
use Fulcrum\Dbal\QueryToken\WhereToken\ComparisonToken;
use Fulcrum\Dbal\QueryToken\WhereToken\InToken;
use Fulcrum\Dbal\QueryToken\WhereToken\LikeToken;
use Fulcrum\Dbal\QueryToken\WhereToken\NotToken;
use Fulcrum\Dbal\QueryToken\WhereToken\OrToken;
use Fulcrum\Dbal\QueryToken\WhereToken\RawToken;
use Fulcrum\Dbal\TableRefContext;

class WhereState implements AbstractState {

	/**
	 * @var AbstractState
	 */
	protected $parentState;
	/**
	 * @var WhereClause
	 */
	protected $where;

    /**
     * @var TableRefContext
     */
	protected $context;

	public function __construct(QueryState $parent, ?TableRefContext $context, WhereClause $clause) {
		$this->parentState = $parent;
		$this->context = $context;
		$this->where = $clause;
		$this->where->setContext($context);
	}

	/* --- Wheres --- */
	public function id($value) {
		return $this->eq(new PrimaryKey(), ComparisonToken::EQ, $value);
	}

	public function eq($field, $value) {
		return $this->addComparison($field, ComparisonToken::EQ, $value);
	}

	public function ieq($field, $value) {
		return $this->addComparison($field, ComparisonToken::IEQ, $value);
	}

	public function gt($field, $value) {
		return $this->addComparison($field, ComparisonToken::GT, $value);
	}

	public function gte($field, $value) {
		return $this->addComparison($field, ComparisonToken::GTE, $value);
	}

	public function lt($field, $value) {
		return $this->addComparison($field, ComparisonToken::LT, $value);
	}

	public function lte($field, $value) {
		return $this->addComparison($field, ComparisonToken::LTE, $value);
	}

	public function isNull($field) {
		return $this->addComparison($field, ComparisonToken::EQ, null);
	}

	public function isNotNull($field) {
		return $this->addComparison($field, ComparisonToken::IEQ, null);
	}

	protected function addComparison($field, $operation, $value) {
		$this->where->addToken(new ComparisonToken($field, $operation, $value));
		return $this;
	}

	public function in($field, $values) {
		$this->where->addToken(new InToken($field, $values));
		return $this;
	}
	public function between($field, $from, $to) {
		$this->where->addToken(new BetweenToken($field,$from,$to));
		return $this;
	}

	public function like($field, $value) {
		$this->addLikeCondition($field, $value, LikeToken::LIKE);
		return $this;
	}
	public function contains($field, $value) {
		return $this->like($field, $value);
	}
	public function begins($field, $value) {
		$this->addLikeCondition($field, $value, LikeToken::BEGINS);
		return $this;
	}
	public function ends($field, $value) {
		$this->addLikeCondition($field, $value, LikeToken::ENDS);
		return $this;
	}

	protected function addLikeCondition($field, $value, $type){
		$this->where->addToken(new LikeToken($field,$value, $type));
	}

	public function where() {
        $whereClause = new WhereClause(true);
        $this->where->addToken($whereClause);
        $whereState = new WhereState($this, $this->context, $whereClause);
        return $whereState;
	}
    public function or() {
        $this->where->addToken(new OrToken());
        return $this;
    }
    public function not() {
        $this->where->addToken(new NotToken());
        return $this;
    }

	public function whereRaw($rawValue) {
		$this->where->addToken(new RawToken($rawValue));
		return $this;
	}

	public function back()
	{
		return $this->parentState;
	}

    public function end() {
        if ($this->parentState && is_a($this->parentState, AbstractState::class)) {
            return $this->parentState->endAll();
        }
        return $this->parentState;
    }

}
