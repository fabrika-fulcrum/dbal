<?php
namespace Fulcrum\Dbal\QueryState;

interface AbstractState extends QueryState {

    public function back();

    public function end();
}
