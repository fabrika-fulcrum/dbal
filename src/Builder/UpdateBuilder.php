<?php

namespace Fulcrum\Dbal\Builder;

use Fulcrum\Dbal\QueryToken\Field;
use Fulcrum\Dbal\QueryToken\OffsetToken;
use Fulcrum\Dbal\QueryToken\OrdersToken;
use Fulcrum\Dbal\QueryToken\Table;
use Fulcrum\Dbal\QueryToken\Token;
use Fulcrum\Dbal\QueryToken\Value;
use Fulcrum\Dbal\QueryToken\WhereClause;

class UpdateBuilder extends AbstractBuilder
{

    /** @var array */
    protected $pairs;
    /** @var Table */
    protected $table;
    /** @var WhereClause */
    protected $where;
    /** @var OrdersToken */
    protected $order;
    /** @var OffsetToken */
    protected $offset;

    public function __construct($connection, $table, $pairs, $where, $order, $offset)
    {
        parent::__construct($connection);

        $this->table = $table;
        $this->pairs = $pairs;
        $this->where = $where;
        $this->order = $order;
        $this->offset = $offset;
    }

    public function build()
    {
        $pairs = [];
        foreach ($this->pairs as $field => $value) {
            $pairs[] =
                (is_a($field, Token::class) ? $field : (new Field($field)))->render($this)
                . ' = ' .
                (is_a($value, Token::class) ? $value : (new Value($value)))->render($this);
        }

        $renderedParts = [
            'UPDATE',
            $this->table->render($this),
            'SET',
            implode(', ', $pairs),
            $this->where->render($this),
            $this->order->render($this),
            $this->offset->render($this)
        ];

        return $this->implodeParts($renderedParts);
    }
}
