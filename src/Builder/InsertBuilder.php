<?php
namespace Fulcrum\Dbal\Builder;

use Fulcrum\Dbal\QueryToken\Field;
use Fulcrum\Dbal\QueryToken\Table;
use Fulcrum\Dbal\QueryToken\Token;
use Fulcrum\Dbal\QueryToken\Value;

class InsertBuilder extends AbstractBuilder {

	/** @var array */
	protected $pairs;
	/** @var  Table */
	protected $table;

	public function __construct($connection, $table, $pairs) {
		parent::__construct($connection);
		$this->table = $table;
		$this->pairs = $pairs;
	}

	public function build() {

		$fields = [];
		$values = [];

		foreach ($this->pairs as $field=>$value) {
			$fields[] = (is_a($field, Token::class)?$field:(new Field($field)))->render($this);
			$values[] = (is_a($value, Token::class)?$value:(new Value($value)))->render($this);
		}

		$renderedParts = [
		    'INSERT INTO',
		    $this->table->render($this),
		    '('.implode(', ',$fields).')',
		    'VALUES',
		    '('.implode(', ',$values).')'
        ];

        return $this->implodeParts($renderedParts);
	}
}
