<?php
namespace Fulcrum\Dbal\Builder;

class RawBuilder extends AbstractBuilder {

	protected $connection;

	protected $table;
	protected $fields;
	protected $joins;
	protected $where;
	protected $order;
	protected $groups;
	protected $offset;

	public function build() {
		return '';
	}
}
