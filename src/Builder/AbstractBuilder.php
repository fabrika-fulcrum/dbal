<?php
namespace Fulcrum\Dbal\Builder;

use Fulcrum\Dbal\Connection;

abstract class AbstractBuilder {

	/** @var  Connection */
	protected $connection;

	public function __construct($connection) {
		$this->connection = $connection;
	}

	public function escape($string) {
		return $this->connection->escape($string);
	}

	public function implodeParts(array $array, string $delimiter=' ') {
        return implode($delimiter,
            array_values(
                array_filter($array,
                    function($value) {
                        return $value !== null;
                    }
                )
            )
        );
    }

	abstract public function build();

}
