<?php
namespace Fulcrum\Dbal\Builder;

use Fulcrum\Dbal\QueryToken\Field;
use Fulcrum\Dbal\QueryToken\FieldsClause;
use Fulcrum\Dbal\QueryToken\GroupsToken;
use Fulcrum\Dbal\QueryToken\Join;
use Fulcrum\Dbal\QueryToken\JoinsClause;
use Fulcrum\Dbal\QueryToken\OffsetToken;
use Fulcrum\Dbal\QueryToken\OrdersToken;
use Fulcrum\Dbal\QueryToken\Table;
use Fulcrum\Dbal\QueryToken\WhereClause;
use Fulcrum\Dbal\QueryToken\WhereToken\WhereToken;

class SelectBuilder extends AbstractBuilder {

	/** @var Table */
	protected $table;
	/** @var FieldsClause */
	protected $fields;
	/** @var JoinsClause */
	protected $joins;
	/** @var WhereClause */
	protected $where;
	/** @var OrdersToken */
	protected $order;
	/** @var GroupsToken */
	protected $groups;
	/** @var OffsetToken */
	protected $offset;

	public function __construct($connection, $table, $fields, $joins, $where, $order, $groups, $offset) {
		parent::__construct($connection);

		$this->table = $table;
		$this->fields = $fields;
		$this->joins = $joins;
		$this->where = $where;
		$this->order = $order;
		$this->groups = $groups;
		$this->offset = $offset;
	}

	public function build() {
		$renderedParts = [
		    'SELECT',
		    $this->fields->render($this),
		    'FROM',
		    $this->table->render($this),
            $this->joins->render($this),
            $this->where->render($this),
            $this->order->render($this),
            $this->groups->render($this),
            $this->offset->render($this)
        ];

        return $this->implodeParts($renderedParts);
	}
}
