<?php
namespace Fulcrum\Dbal\Builder;

use Fulcrum\Dbal\QueryToken\Field;
use Fulcrum\Dbal\QueryToken\OffsetToken;
use Fulcrum\Dbal\QueryToken\OrdersToken;
use Fulcrum\Dbal\QueryToken\Table;
use Fulcrum\Dbal\QueryToken\Value;
use Fulcrum\Dbal\QueryToken\WhereClause;

class DeleteBuilder extends AbstractBuilder {

	/** @var Table */
	protected $table;
	/** @var WhereClause */
	protected $where;
	/** @var OrdersToken */
	protected $order;
	/** @var OffsetToken */
	protected $offset;

	protected $values;

	public function __construct($connection, $table, $where, $order, $offset) {
		parent::__construct($connection);

		$this->table = $table;
		$this->where = $where;
		$this->order = $order;
		$this->offset = $offset;
	}

	public function build() {
		$renderedParts = [];

		$renderedParts[] = 'DELETE FROM';
		$renderedParts[] = $this->table->render($this);
		if ($this->where->hasClauses()) {
			$renderedParts[] = $this->where->render($this);
		}
		if ($this->order->hasClauses()) {
			$renderedParts[] = $this->order->render($this);
		}
		if ($this->offset->hasClauses()) {
			$renderedParts[] = $this->offset->render($this);
		}
		return implode(' ',$renderedParts);
	}
}
