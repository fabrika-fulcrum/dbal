<?php

namespace Fulcrum\Dbal;

use Fulcrum\Dbal\QueryToken\Table;

interface TableRefContext
{
    public function _getTable():Table;
}
