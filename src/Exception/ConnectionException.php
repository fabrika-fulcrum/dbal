<?php
namespace Fulcrum\Dbal\Exception;

class ConnectionException extends DbalException {}
